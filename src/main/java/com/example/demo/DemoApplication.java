package com.example.demo;

import com.microsoft.cognitiveservices.speech.*;
import org.apache.catalina.webresources.TomcatJarInputStream;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.context.Theme;
import org.springframework.web.client.RestTemplate;

import javax.management.MXBean;
import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) throws IOException, InterruptedException {

        // Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://www.google.com/"});
        // Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://www.trendyol.com/"});

        // Process spotify = Runtime.getRuntime().exec("/Applications/Spotify.app/Contents/MacOS/Spotify");
        // spotify.waitFor();


        Process whatsapp = Runtime.getRuntime().exec("/Applications/WhatsApp.app/Contents/MacOS/WhatsApp");


        // ConfigurableApplicationContext run = SpringApplication.run(DemoApplication.class, args);

        // Process safari = Runtime.getRuntime().exec(new String[] {"/Applications/Safari.app/Contents/MacOS/Safari",  "www.google.com" });
        //Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://yourwebsite.com/"});

        // Process spotify = Runtime.getRuntime().exec("/Applications/Spotify.app/Contents/MacOS/Spotify");
        // exec.waitFor();
    }

    @Bean
    public void init()
    {
        System.out.println("init");
    }

}

/**
 * Quickstart: recognize speech using the Speech SDK for Java.
 */
class Main {

    /**
     * @param args Arguments are ignored in this sample.
     */
    public static void main(String[] args) {
        try {
            // Replace below with your own subscription key
            String speechSubscriptionKey = "bae3bb2efe334dc78131bb663a9a73a1";
            // Replace below with your own service region (e.g., "westus").
            String serviceRegion = "francecentral";

            int exitCode = 1;
            SpeechConfig config = SpeechConfig.fromSubscription(speechSubscriptionKey, serviceRegion);
            assert(config != null);

            SpeechRecognizer reco = new SpeechRecognizer(config);
            assert(reco != null);

            System.out.println("Say something...");

            while (true) {
                Future<SpeechRecognitionResult> task = reco.recognizeOnceAsync();
                assert(task != null);

                SpeechRecognitionResult result = task.get();
                assert(result != null);

                if (result.getReason() == ResultReason.RecognizedSpeech) {
                    System.out.println("We recognized: " + result.getText());

                    if (result.getText().toLowerCase().contains("test")) {
                        SpeechSynthesizer synth = new SpeechSynthesizer(config);
                        assert(synth != null);

                        System.out.println("Type some text that you want to speak...");
                        System.out.print("> ");
                        String text = new Scanner(System.in).nextLine();

                        Future<SpeechSynthesisResult> task1 = synth.SpeakTextAsync(text);
                        assert(task1 != null);

                        SpeechSynthesisResult result1 = task1.get();
                        assert(result1 != null);

                        if (result1.getReason() == ResultReason.SynthesizingAudioCompleted) {
                            System.out.println("Speech synthesized to speaker for text [" + text + "]");
                            exitCode = 0;
                        }
                        else if (result1.getReason() == ResultReason.Canceled) {
                            SpeechSynthesisCancellationDetails cancellation = SpeechSynthesisCancellationDetails.fromResult(result1);
                            System.out.println("CANCELED: Reason=" + cancellation.getReason());

                            if (cancellation.getReason() == CancellationReason.Error) {
                                System.out.println("CANCELED: ErrorCode=" + cancellation.getErrorCode());
                                System.out.println("CANCELED: ErrorDetails=" + cancellation.getErrorDetails());
                                System.out.println("CANCELED: Did you update the subscription info?");
                            }
                        }
                    }

                    if (result.getText().toLowerCase().contains("weather")) {
                        String city = result.getText().split(" ")[1].replace(".", "").toLowerCase();
                        String url = WeatherApiUrl.getWeatherApiUrlForJson(city);

                        ResponseEntity<HashMap> response = new RestTemplate().getForEntity(url, HashMap.class);

                        HashMap mapsData = response.getBody();

                        HashMap mapsOfCurrent = (HashMap) mapsData.get("current");
                        Double temp_c = (Double) mapsOfCurrent.get("temp_c");
                        String s = String.valueOf(temp_c);
                        System.out.println(temp_c);


                        SpeechSynthesizer synth = new SpeechSynthesizer(config);
                        assert(synth != null);

                        System.out.println("Type some text that you want to speak...");
                        System.out.print("> ");
                        String text = city + temp_c + "derece";

                        Future<SpeechSynthesisResult> task1 = synth.SpeakTextAsync(text);// 10sanye


                        assert(task1 != null);

                        SpeechSynthesisResult result1 = task1.get();
                        assert(result1 != null);

                        if (result1.getReason() == ResultReason.SynthesizingAudioCompleted) {
                            System.out.println("Speech synthesized to speaker for text [" + text + "]");
                            exitCode = 0;
                        }


                        else if (result1.getReason() == ResultReason.Canceled) {
                            SpeechSynthesisCancellationDetails cancellation = SpeechSynthesisCancellationDetails.fromResult(result1);
                            System.out.println("CANCELED: Reason=" + cancellation.getReason());

                            if (cancellation.getReason() == CancellationReason.Error) {
                                System.out.println("CANCELED: ErrorCode=" + cancellation.getErrorCode());
                                System.out.println("CANCELED: ErrorDetails=" + cancellation.getErrorDetails());
                                System.out.println("CANCELED: Did you update the subscription info?");
                            }
                        }
                    }
                    if (result.getText().toLowerCase().contains("spotify.")) {
                        String tarkan = result.getText().toLowerCase().split(" ")[1];
                        // Process exec = Runtime.getRuntime().exec("/Applications/Spotify.app/Contents/MacOS/Spotify");
                        SpotifyService.run();
                    }

                    if (result.getText().toLowerCase().contains("production inventory database")) {
                        System.out.println("http://10.10.36.120:8091/ui/index.html#!/buckets?scenarioBucket=Campaign&scenarioZoom=minute");
                    }

                    if (result.getText().toLowerCase().contains("production stock database.")) {
                        Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://10.250.58.10:8091/ui/index.html#!/index?scenarioBucket=Stockholm&scenarioZoom=hour"});
                        System.out.println("http://10.250.58.10:8091/ui/index.html#!/index?scenarioBucket=Stockholm&scenarioZoom=hour");
                    }

                    if (result.getText().toLowerCase().contains("open")) {
                        Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "https://www.trendyol.com/butik/liste/erkek"});
                        System.out.println("http://10.250.58.10:8091/ui/index.html#!/index?scenarioBucket=Stockholm&scenarioZoom=hour");
                    }

                    if (result.getText().toLowerCase().contains("me")) {
                        Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "https://pairfy-cdn.fra1.digitaloceanspaces.com/tugberk.jpg"});
                        System.out.println("http://10.250.58.10:8091/ui/index.html#!/index?scenarioBucket=Stockholm&scenarioZoom=hour");
                    }

                    if (result.getText().toLowerCase().contains("brother")) {
                        Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://image.pairfy.team:8080/bugra"});
                        System.out.println("http://10.250.58.10:8091/ui/index.html#!/index?scenarioBucket=Stockholm&scenarioZoom=hour");
                    }

                    if (result.getText().toLowerCase().contains("spotify")) {
                        /// Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://image.pairfy.team:8080/bugra"});
                        // System.out.println("http://10.250.58.10:8091/ui/index.html#!/index?scenarioBucket=Stockholm&scenarioZoom=hour");
                        System.out.println("tarkan");
                        //SpotifyService.run();
                    }

                    exitCode = 0;
                }
                else if (result.getReason() == ResultReason.NoMatch) {
                    System.out.println("NOMATCH: Speech could not be recognized.");
                }
                   else if (result.getReason() == ResultReason.Canceled) {
                    CancellationDetails cancellation = CancellationDetails.fromResult(result);
                    System.out.println("CANCELED: Reason=" + cancellation.getReason());

                    if (cancellation.getReason() == CancellationReason.Error) {
                        System.out.println("CANCELED: ErrorCode=" + cancellation.getErrorCode());
                        System.out.println("CANCELED: ErrorDetails=" + cancellation.getErrorDetails());
                        System.out.println("CANCELED: Did you update the subscription info?");
                    }
                }

                // reco.close();

                // System.exit(exitCode);
            }

        } catch (Exception ex) {
            System.out.println("Unexpected exception: " + ex.getMessage());

            assert(false);
            // System.exit(1);
        }
    }
}
// </code>



class SpotifyService {

    public static void main(String[] args) throws InterruptedException {
        Properties properties = System.getProperties();
        System.out.println(System.getProperty("java.home"));
    }

    //@org.junit.jupiter.api.Test
    public static void run() throws InterruptedException {
        // Optional. If not specified, WebDriver searches the PATH for chromedriver.
        System.setProperty("webdriver.chrome.driver", "chromedriver");

        WebDriver driver = new ChromeDriver();
        driver.get("https://accounts.spotify.com/en/login?continue=https:%2F%2Fopen.spotify.com%2F");
        Thread.sleep(1000);  // Let the user actually see something!
        WebElement searchBox = driver.findElement(By.id("login-username"));
        WebElement searchBox1 = driver.findElement(By.id("login-password"));
        WebElement loginButton = driver.findElement(By.id("login-button"));
        searchBox.sendKeys(System.getProperty("login-email"));
        searchBox1.sendKeys("xxxxxx"); // todo:enter pass
        loginButton.click();
        Thread.sleep(2000);
        driver.navigate().to("https://open.spotify.com/search/tarkan");
        Thread.sleep(2000);  // Let the user actually see something!
        WebElement nowPlayButton = driver.findElement(By.className("_82ba3fb528bb730b297a91f46acd37a3-scss")); // todo: hard coded
        nowPlayButton.click();
        Thread.sleep(1000);  // Let the user actually see something!
        // driver.quit();
    }
}


class WeatherApiUrl {

    public static void main(String[] args) {

    }
    // http://api.weatherapi.com/v1/current.json?key=3a285091fce743c0a05100233200110&q=İstanbul http call yapılacak url
    public static String getWeatherApiUrlForJson(String cityName) {
        return String.format("http://api.weatherapi.com/v1/current.json?key=3a285091fce743c0a05100233200110&q=%s", cityName);
    }

    public static String getWeatherApiUrlForXml(String cityName) {
        return String.format("http://api.weatherapi.com/v1/current.xml?key=3a285091fce743c0a05100233200110&q=%s", cityName);
    }
}





class AsyncCall {

    public static void main(String[] args) throws IOException, InterruptedException {
        call("ASDA");
        System.out.println("asdasd");
    }

    public static void call(String bugra) throws InterruptedException, IOException
    {
        Runnable code = () -> {
            while(true)
            {
                File file;
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(new File("bugra.txt"), true);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    fos.write(bugra.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fos.write("\n".getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        new Thread(code).start();
        System.out.println("devam");
    }
}


class App {
    public App() {
    }

    public static void main(String[] args) throws InterruptedException {

        System.out.println("MAİN ");

        Thread spotifyOpener= new Thread(() -> {
            try {
                Process spotify = Runtime.getRuntime().exec("/Applications/Spotify.app/Contents/MacOS/Spotify");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Thread chromeOpener= new Thread(() -> {
            while (true) {
                try {
                    System.out.println("chrome opener");
                    Process chrom = Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://www.google.com/"});
                    Thread.sleep(4000);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        spotifyOpener.join();
        chromeOpener.start();

        while (true) {
            new Thread(() -> { while (true) System.out.println("yazdi");}).start();
            try {
                System.out.println("main açıyor");
                Process chrom = Runtime.getRuntime().exec(new String[]{"/usr/bin/open", "-a", "/Applications/Google Chrome.app", "http://www.google.com/"});
                Thread.sleep(4000);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }



    }
}
